import "./App.css";

function App() {
  const weekdays = [
    "Maanantai",
    "Tiistai",
    "Keskiviikko",
    "Torstai",
    "Perjantai",
    "Lauantai",
    "Sunnuntai",
  ];
  const returnWeekdays = weekdays.map((day, position) => {
    return <p>{day}</p>;
  });
  return <div className="weekdays">Weekday{returnWeekdays}</div>;
}

export default App;
